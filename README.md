# Schüttelerkennung mit Library

[safetysystemtechnology/android-shake-detector](safetysystemtechnology/android-shake-detector)



## Erlaubnis für den Sensor in der AndroidManifest.xml

```Xml
<uses-feature android:name="android.hardware.sensor.accelerometer" android:required="true" />
```

## In der Applikations `build.gradle`

